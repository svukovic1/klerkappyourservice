<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
Route::model('user', 'User');
Route::model('comment', 'Comment');
Route::model('post', 'Post');
Route::model('role', 'Role');
Route::model('vehicle', 'Vehicle');

/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('comment', '[0-9]+');
Route::pattern('post', '[0-9]+');
Route::pattern('user', '[0-9]+');
Route::pattern('role', '[0-9]+');
Route::pattern('token', '[0-9a-z]+');

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'admin', 'before' => 'auth'), function() {

    Route::get('users?picked_up=true', 'AdminUsersController@getData');
     Route::get('users/service-moment', 'AdminUsersController@show');
    Route::get('users/service', 'AdminUsersController@getServiceData');
    Route::get('users/create/autocomplete', 'AdminUsersController@autocomplete');

    # User Management
    Route::post('users/store', 'AdminUsersController@store');
    Route::get('users/{user}/show', 'AdminUsersController@getShow');
    Route::get('users/{user}/edit', 'AdminUsersController@getEdit');
    Route::post('users/{user}/edit', 'AdminUsersController@postEdit');
    Route::get('users/{user}/delete', 'AdminUsersController@postDelete');
//    Route::post('users/{user}/delete', 'AdminUsersController@postDelete');
    Route::get('users/delete/{id}', 'AdminUsersController@destroy');
    Route::controller('users', 'AdminUsersController');

    # CoAdmin Management
    Route::get('coAdmin/{user}/show', 'CoAdminController@getShow');
    Route::get('coAdmin/{user}/edit', 'CoAdminController@getEdit');
    Route::post('coAdmin/{user}/edit', 'CoAdminController@postEdit');
    Route::get('coAdmin/{user}/delete', 'CoAdminController@getDelete');
    Route::post('coAdmin/{user}/delete', 'CoAdminController@postDelete');
    Route::controller('coAdmin', 'CoAdminController');

    # User Role Management
    Route::get('roles/{role}/show', 'AdminRolesController@getShow');
    Route::get('roles/{role}/edit', 'AdminRolesController@getEdit');
    Route::post('roles/{role}/edit', 'AdminRolesController@postEdit');
    Route::get('roles/{role}/delete', 'AdminRolesController@getDelete');
    Route::post('roles/{role}/delete', 'AdminRolesController@postDelete');
    Route::controller('roles', 'AdminRolesController');

    # Vehicle Management    

    Route::controller('vehicle', 'AdminRolesController');


    # Admin Dashboard
    Route::controller('/', 'AdminDashboardController');
});


/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */
// User reset routes
Route::get('user/reset/{token}', 'UserController@getReset');
// User password reset
Route::post('user/reset/{token}', 'UserController@postReset');
//:: User Account Routes ::
Route::post('user/{user}/edit', 'UserController@postEdit');


Route::post('/user/date', 'UserController@postDate');

//:: User Account Routes ::
Route::post('user/login', 'UserController@postLogin');

Route::get('/', array('uses' => 'UserController@postLoginEmail'));
Route::post('/', 'UserController@postLoginEmail');

Route::get('/trailer', 'UserController@');
//:: User Account Routes ::
//Route::post('admin/login', 'UserController@postLogin');
# User RESTful Routes (Login, Logout, Register, etc)
Route::controller('user', 'UserController');

//:: Application Routes ::
# Filter for detect language
Route::when('contact-us', 'detectLang');

# Contact Us Static Page
Route::get('contact-us', function() {
    // Return about us page
    return View::make('site/contact-us');
});

# Posts - Second to last set, match slug
Route::get('{postSlug}', 'BlogController@getView');
Route::post('{postSlug}', 'BlogController@postView');

# Index Page - Last route, no matches
Route::get('/', array('before' => 'detectLang', 'uses' => 'UserController@getIndex'));


