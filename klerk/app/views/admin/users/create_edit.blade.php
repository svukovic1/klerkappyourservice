@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
<!-- Tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#trailers" aria-controls="trailers" role="tab" data-toggle="tab">Trailers</a></li>
    @if($mode == 'edit')    
    @foreach($vehicles as $key => $vehicle)              

    <li role="presentation"><a href="#vehicles{{ $key + 1 }}" aria-controls="vehicles" role="tab" data-toggle="tab">Vehicle{{ $key + 1 }}</a></li>

    @endforeach

    @else

    <li role="presentation"><a href="#vehicles" aria-controls="vehicles" role="tab" data-toggle="tab">Vehicle</a></li>

    @endif
    <li><a href="#" class="add-contact">+ Add Vehicle</a></li>
</ul>
<!-- ./ tabs -->

{{-- Create User Form --}}
<form class="form-horizontal" id="trailerForm" autocomplete="off" method="post">

    <!-- Tabs Content -->
    <div class="tab-content">
        <!-- General tab -->
        <div role="tabpanel" class="tab-pane active" id="trailers">

            <!-- CSRF Token -->
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            <!-- ./ csrf token -->
			<!-- user id -->
			<input type="hidden" name="id" value="{{{ Input::old('id', isset($user) ? $user->id : null) }}}" />
            <!-- firstName -->
            <div class="form-group {{{ $errors->has('firstName') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="firstName">{{--First Name--}}Debit number</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" name="firstName" id="firstName" value="{{{ Input::old('firstName', isset($user) ? $user->firstName : null) }}}" />
                    {{ $errors->first('firstName', '<span class="help-inline">:message</span>') }}
                    @if(isset($user))
                    <input type="hidden" name="code" value="" />
                    @endif
                </div>
            </div>
            <!-- ./ firstName -->

            <!-- lastName -->
            <div class="form-group {{{ $errors->has('lastName') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="lastName">Last Name</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" name="lastName" id="lastName" value="{{{ Input::old('lastName', isset($user) ? $user->lastName : null) }}}" />
                    {{ $errors->first('lastName', '<span class="help-inline">:message</span>') }}
                </div>
            </div>
            <!-- ./ lastName -->

            <!-- phone -->
            <div class="form-group {{{ $errors->has('phone') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="phone">Phone</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" name="phone" id="phone" value="{{{ Input::old('phone', isset($user) ? $user->phone : null) }}}" />
                    {{ $errors->first('phone', '<span class="help-inline">:message</span>') }}
                </div>
            </div>
            <!-- ./ phone -->
            
             <!-- mobile -->
            <div class="form-group {{{ $errors->has('mobile') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="mobile">Mobile</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" name="mobile" id="phone" value="{{{ Input::old('mobile', isset($user) ? $user->mobile : null) }}}" />
                    {{ $errors->first('mobile', '<span class="help-inline">:message</span>') }}
                </div>
            </div>
            <!-- ./ mobile -->

            <!-- street -->
            <div class="form-group {{{ $errors->has('street') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="street">Street</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" name="street" id="street" value="{{{ Input::old('street', isset($user) ? $user->street : null) }}}" />
                    {{ $errors->first('street', '<span class="help-inline">:message</span>') }}
                </div>
            </div>
            <!-- ./ street -->

            <!-- city -->
            <div class="form-group {{{ $errors->has('city') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="city">City</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" name="city" id="city" value="{{{ Input::old('city', isset($user) ? $user->city : null) }}}" />
                    {{ $errors->first('city', '<span class="help-inline">:message</span>') }}
                </div>
            </div>
            <!-- ./ city -->

            <!-- zip -->
            <div class="form-group {{{ $errors->has('zip') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="zip">Postal code</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" name="zip" id="zip" value="{{{ Input::old('zip', isset($user) ? $user->zip : null) }}}" />
                    {{ $errors->first('zip', '<span class="help-inline">:message</span>') }}
                </div>
            </div>
            <!-- ./ zip -->



            <!-- Email -->
            <div class="form-group {{{ $errors->has('email') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="email">Email</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" name="email" {{-- @if(isset($user)) readonly @endif --}} id="email" value="{{{ Input::old('email', isset($user) ? $user->email : null) }}}" />
                    {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
                </div>
            </div>
            <!-- ./ email -->



            <!-- Form Actions -->



        </div>
        @if($mode == 'edit')


        @foreach($vehicles as $key => $vehicle)
        @include('includes.tabs', array('key' => $key,'vehicle' => $vehicle))
        @endforeach    
        </div>
    <!-- ./ tabs content -->
    <div class="right">
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <a class="btn btn-default btn-small btn-inverse" href="{{ URL::to('admin/users') }}">Cancel</a>
                <button type="submit" id="submitForm" class="btn btn-success">Save</button>
            </div>
        </div>
</form> 


        @else
        @include('includes.tabs')

    </div>
    <!-- ./ tabs content -->
    <div class="right">
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <a class="btn btn-default btn-small btn-inverse back" href="{{ URL::to('admin/users') }}">Cancel</a>
                <button type="submit" id="submitForm" class="btn btn-success register">Register</button>
            </div>
        </div>
</form> 
</div>
</form>

<!-- ./ form actions -->
@endif
@stop
