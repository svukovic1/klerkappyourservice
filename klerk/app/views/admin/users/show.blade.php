@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ $title }}} :: @parent
@stop

{{-- Content --}}
@section('content')

<div class="page-header">
    <h2>
        {{{ $title }}}
       
        <div class="pull-right">
            
            <a href="{{{ URL::to('admin/users/') }}}" class="btn btn-small btn-info "><span class="glyphicon glyphicon-arrow-left"></span> Back</a>


        </div>
    </h2>
</div>

<table id="users" class="table table-striped table-hover">
    <thead>
        <tr>
           
            <th class="col-md-2">{{{ Lang::get('admin/users/table.first_name') }}}</th>
            <th class="col-md-2">{{{ Lang::get('admin/users/table.last_name') }}}</th>
            <th class="col-md-2">{{{ Lang::get('admin/users/table.trailerType') }}}</th>
            <th class="col-md-2">{{{ Lang::get('admin/users/table.location') }}}</th>
            <th class="col-md-1">{{{ Lang::get('admin/users/table.pick_up_date') }}}</th>
            <th class="col-md-1">{{{ Lang::get('admin/users/table.confirm') }}}</th>
            <th class="col-md-1">{{{ Lang::get('admin/users/table.service') }}}</th>


        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@stop

{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
    var oTable;
    $(document).ready(function () {
        oTable = $('#users').dataTable({
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ records per page"
            },
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "/admin/users/service",
            "fnDrawCallback": function (oSettings) {
                $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});


            }


        });

    });
</script>
@stop