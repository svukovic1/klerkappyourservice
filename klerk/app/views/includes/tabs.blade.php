<!-- VEHICLE TAB -->
<div role="tabpanel" class="tab-pane" @if(isset($key)) id="vehicles{{ $key + 1 }}" @else id="vehicles" @endif>
  

    <!-- type of trailer -->
    <div class="form-group {{{ $errors->has('trailerType') ? 'error' : '' }}}">
        <label class="col-md-2 control-label" for="trailerType">Type of trailer</label>
        <div class="col-md-10">
            <input class="form-control" type="text" name="trailerType[]"   id="trailerType" value="{{{ Input::old('trailerType', isset($vehicle) ? $vehicle->trailerType : null) }}}"  />
            <input type="hidden" name="vehicleID[]" @if(isset($key)) value="{{ $key }}" @else  value="0" @endif />
            {{ $errors->first('trailerType', '<span class="help-inline">:message</span>') }}
            
        </div>
    </div>
    <!-- ./ type of trailer -->

    <!-- Delivery date -->
    <?php
    if(isset($vehicle)) {
        $deliveryDate = date_create($vehicle->deliveryDate);
        if($vehicle->serviceMoment !== NULL ) {
            $serviceMoment = date_create($vehicle->serviceMoment);
        }
        $pickupDate = date_create($vehicle->pickupDate); 
    }
    ?>
    <div class="form-group {{{ $errors->has('deliveryDate') ? 'error' : '' }}}">
        <label class="col-md-2 control-label" for="deliveryDate">Delivery date</label>
        <div  class="col-md-3 input-group date" @if(isset($key)) id="deliveryDate{{ $key + 1 }}" @else id="deliveryDate" @endif>
            <input  type='text' @if(isset($key)) id="deliveryDate{{ $key + 1 }}" @else id="deliveryDate" @endif name="deliveryDate[]"  class="form-control" value="{{{ Input::old('deliveryDate', isset($vehicle) ?  date_format($deliveryDate, 'd/m/Y')  : null) }}}" />
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            </span>            
        </div>
    </div>
    <!-- ./ Delivery date -->

    <!-- Location of storage -->
    <div class="form-group {{{ $errors->has('locationStorage') ? 'error' : '' }}}">
        <label class="col-md-2 control-label" for="street">Location of storage</label>
        <div class="col-md-10">
            <input class="form-control search" type="text"  name="locationStorage[]"  id="locationStorage" value="{{{ Input::old('locationStorage', isset($vehicle) ? $vehicle->locationStorage : null) }}}" />
            {{ $errors->first('locationStorage', '<span class="help-inline">:message</span>') }}
        </div>
    </div>
    <!-- ./ Location of storage -->

    <!-- service moment -->
    <div class="form-group {{{ $errors->has('serviceMoment') ? 'error' : '' }}}">
        <label class="col-md-2 control-label" for="serviceMoment">Set Service moment</label>
         <div  class="col-md-3 input-group date" @if(isset($key)) id="serviceMoment{{ $key + 1 }}"@else id="serviceMoment" @endif>
            <input  type='text' @if(isset($key)) id="serviceMoment{{ $key + 1 }}"@else id="serviceMoment" @endif   name="serviceMoment[]" class="form-control" value="{{{ Input::old('serviceMoment', isset($vehicle) && isset($serviceMoment)  ? date_format($serviceMoment, 'd/m/Y') : null) }}}" />
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            </span>            
        </div>
    </div>
    <!-- ./ service moment -->

    <!-- license -->
    <div class="form-group {{{ $errors->has('license') ? 'error' : '' }}}">
        <label class="col-md-2 control-label" for="license">License</label>
        <div class="col-md-10">
            <input class="form-control" type="text"  name="license[]"  id="license" value="{{{ Input::old('license', isset($vehicle) ? $vehicle->license : null) }}}" />
            {{ $errors->first('license', '<span class="help-inline">:message</span>') }}
        </div>
    </div>
    <!-- ./ license -->
    
    <!-- park for year -->
    <div class="form-group {{{ $errors->has('park') ? 'error' : '' }}}">
        <label class="col-md-2 control-label" for="park">Park for year</label>
        <div class="btn-group date" data-toggle="buttons">
            <select class="form-control" name="park[]">
                @if(isset($vehicle))
                <option value="1" @if(isset($vehicle) && $vehicle->park == 1) selected="selected" @endif >Yes</option>
                <option value="0" @if(isset($vehicle) && $vehicle->park == 0) selected="selected" @endif >No</option>
                @else
                <option value="1" >Yes</option>
                <option value="0" selected="selected">No</option>
                @endif
            </select>
        </div>
    </div>
    <!-- ./ park for year -->
    
    <!-- Pick up date -->
    <div class="form-group {{{ $errors->has('pickupDate') ? 'error' : '' }}}">
        <label class="col-md-2 control-label" for="pickupDate">Pickup date</label>
        <div  class="col-md-3 input-group date" @if(isset($key)) id="pickupDate{{ $key + 1}}" @else id="pickupDate" @endif>
			<input  type="text" @if(isset($key)) id="pickupDate{{ $key + 1}}" @else id="pickupDate" @endif  name="pickupDate[]"   class="form-control" value="{{{ Input::old('pickupDate', isset($vehicle) ? (date_format($pickupDate, 'Y') != '-0001'  ? date_format($pickupDate, 'd/m/Y') : null) : null) }}}" />
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            </span>            
        </div>
    </div>
    <!-- ./ Pick up date -->

    <!-- Comments -->
    <div class="form-group {{{ $errors->has('comments') ? 'error' : '' }}}">
        <label class="col-md-2 control-label" for="comments">Comment</label>
        <div class="col-md-10">
            <textarea  name="comments[]" class="form-control" rows="3">{{{ Input::old('comments', isset($vehicle) ? $vehicle->comments : null) }}}</textarea>
        </div>
    </div>
    <!-- ./ comments -->
    
     <!-- confirm pick up -->
     <div class="form-group"> 
        <label class="col-md-2 control-label" for="comments">Confirm Pick up</label>
        <div class="checkbox col-md-10 date">
            <input type="hidden" @if(isset($key)) name="pick_up[{{$key}}]" @else name="pick_up[0]" @endif value="0">
            <input type="checkbox" @if(isset($key)) name="pick_up[{{$key}}]" @else name="pick_up[0]" @endif @if(isset($vehicle->pick_up) && $vehicle->pick_up == 1 ) checked="checked" @endif value="1">
                   
        </div>    
    </div>
    <!-- ./ confirm pick up --> 
    @if(isset($vehicle))
    
    <div class="form-group"> 
        <label class="col-md-2 control-label" for="comments">Delete vehicle?</label>
        <div class="col-md-10 date">       
            <a href="{{ action('AdminUsersController@destroy', isset($vehicle) ? $vehicle->id : null)}}" class="btn  btn-danger" onclick="return confirm('Are you sure you want to delete this vehicle?')">{{{ Lang::get('button.delete') }}}</a> 
        </div>    
    </div>
    @endif
    


</div>
<!-- ./ END VEHICLE TAB -->

