<?php

return array(

	'first_name' => 'First Name',
        'last_name' => 'Last Name',
	'trailerType'  => 'Type of caravan',
	'user_id'  => 'User Id',
	'location'  => 'Location',
	'pick_up_date'      => 'Pick up date',
	'confirm'     => 'Confirm pick-up',
	'roles'     => 'Roles',
	'activated'  => 'Activated',
	'created_at' => 'Created at',
        'count' => 'Pick-up counter',
    'service' => 'Service Moment',

);
