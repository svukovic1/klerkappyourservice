<?php

return array(
    'first_name' => 'Voornaam',
    'last_name' => 'Achternaam',
    'city' => 'Plaats',
    'street' => 'Straat',
    'postal_code' => 'Postcode',
    'email' => 'Email',
    'phone' => 'Telefoonnummer',
);
