<?php

class AdminUsersController extends AdminController
{

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Role Model
     * @var Role
     */
    protected $role;

    /**
     * Permission Model
     * @var Permission
     */
    protected $permission;

    /**
     * Vehicle Model
     * @var Vehicle
     */
    protected $vehicles;

    /**
     * Inject the models.
     * @param User $user
     * @param Role $role
     * @param Permission $permission
     */
    public function __construct(User $user, Role $role, Permission $permission, Vehicle $vehicles)
    {
        parent::__construct();
        $this->user = $user;
        $this->role = $role;
        $this->permission = $permission;
        $this->vehicle = $vehicles;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/users/title.user_management');
        // URL get
        $pick_up = Input::get('picked_up');
        $success = Input::get('success');
        // Grab all the users
        $users = $this->user;

        $vehicles = $this->vehicle->all();
        foreach ($vehicles as $vehicle) {
            $pickupDate = strtotime(date_format(date_create($vehicle->pickupDate), 'd-m-Y'));
            $curentDate = strtotime(date('d-m-Y'));

            // if pick-up date has passed send mail
            // if ($vehicle->pick_up == 0 && $pickupDate < $curentDate) {
			// If pick-up is today send mail 
			if ($pickupDate == $curentDate) {

                $users = User::where('id', $vehicle->user_id)->get();
                foreach ($users as $user) {
                    $date = date_format(date_create($vehicle->pickupDate), 'd-m-Y');
                    // SEND MAIL
                    Mail::send('emails.pickUpDate', array('firstname' => $user->firstName, 'lastname' => $user->lastName, 'email' => $user->email
                        , 'phone' => $user->phone, 'date' => $date, 'trailerType' => $vehicle->trailerType,
                        'locationStorage' => $vehicle->locationStorage, 'license' => $vehicle->license), function($message) {
                        $message->to('info@deklerkcaravans.nl', 'DeKlerkCaravans')->subject('Pick-up date has passed');
                    });
                }
            }
        }


        // Show the page
        return View::make('admin/users/index', compact('users', 'title', 'pick_up', 'success'));
    }

    public function show()
    {
        // Title
        $title = Lang::get('admin/users/title.service');

        // Grab all the users
        $users = $this->user;

        // Show the page
        return View::make('admin/users/show', compact('users', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        // All roles
        $roles = $this->role->all();

        // Get all the available permissions
        $permissions = $this->permission->all();

        // Selected groups
        $selectedRoles = Input::old('roles', array());

        // Selected permissions
        $selectedPermissions = Input::old('permissions', array());

        // Title
        $title = Lang::get('admin/users/title.create_a_new_user');

        // Mode
        $mode = 'create';

        // Show the page
        return View::make('admin/users/create_edit', compact('roles', 'permissions', 'selectedRoles', 'selectedPermissions', 'title', 'mode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {


        dd(Input);
        $this->user->username = Input::get('username');
        $this->user->email = Input::get('email');
        $this->user->password = Input::get('password');
        $this->user->firstName = Input::get('firstName');
        $this->user->lastName = Input::get('lastName');
        $this->user->street = Input::get('street');
        $this->user->zip = Input::get('zip');
        $this->user->city = Input::get('city');

        // The password confirmation will be removed from model
        // before saving. This field will be used in Ardent's
        // auto validation.
        $this->user->password_confirmation = Input::get('password_confirmation');

        // Generate a random confirmation code
        $this->user->confirmation_code = md5(uniqid(mt_rand(), true));

        if (Input::get('confirm')) {
            $this->user->confirmed = Input::get('confirm');
        }

        // Permissions are currently tied to roles. Can't do this yet.
        //$user->permissions = $user->roles()->preparePermissionsForSave(Input::get( 'permissions' ));
        // Save if valid. Password field will be hashed before save


        $this->user->save();

        if ($this->user->id) {
            // Save roles. Handles updating.
            $roles = array(2);
            $this->user->saveRoles($roles);

//            if (Config::get('confide::signup_email')) {
//                $user = $this->user;
//                Mail::queueOn(
//                    Config::get('confide::email_queue'),
//                    Config::get('confide::email_account_confirmation'),
//                    compact('user'),
//                    function ($message) use ($user) {
//                        $message
//                            ->to($user->email, $user->username)
//                            ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
//                    }
//                );
//            }
            // Redirect to the new user page
            return Redirect::to('admin/users/' . $this->user->id . '/edit')
                            ->with('success', Lang::get('admin/users/messages.create.success'));
        } else {

            // Get validation errors (see Ardent package)
            $error = $this->user->errors()->all();

            return Redirect::to('admin/users/create')
                            ->withInput(Input::except('password'))
                            ->with('error', $error);
        }
    }

    public function store()
    {


        if (Request::ajax()) {


            $rules = array();
            foreach (Input::get('vehicleID') as $key => $value) {

                $fields['trailerType' . $value] = Input::get('trailerType')[$value];
                $fields['license' . $value] = Input::get('license')[$value];
                $fields['locationStorage' . $value] = Input::get('locationStorage')[$value];
                $fields['deliveryDate' . $value] = Input::get('deliveryDate')[$value];
                $fields['serviceMoment' . $value] = Input::get('serviceMoment')[$value];
                $fields['pickupDate' . $value] = Input::get('pickupDate')[$value];
                $fields['firstName'] = Input::get('firstName');
                $fields['lastName'] = Input::get('lastName');
                $fields['email'] = Input::get('email');
                $fields['phone'] = Input::get('phone');
                $fields['mobile'] = Input::get('mobile');
                $fields['street'] = Input::get('street');
                $fields['city'] = Input::get('city');


                $rules['firstName'] = 'required';
                $rules['lastName'] = 'required';
                $rules['phone'] = 'min:5';
                $rules['mobile'] = 'min:5';
                $rules['street'] = 'required|min:5';
                $rules['city'] = 'required';
                $exists = Input::get('code');
                if (isset($exists)) {
                    $rules['email'] = 'required|email';
                } else {
                    $rules['email'] = 'required|unique:users';
                }
				//$rules['license' . $value] = 'required';
                $rules['trailerType' . $value] = 'required';

                $rules['locationStorage' . $value] = 'required';
                $rules['deliveryDate' . $value] = 'required';
//                    $rules['serviceMoment'.$value] = 'required';
                //$rules['pickupDate' . $value] = 'required';

                $messages = array(
                    'trailerType' . $value . '.required' => 'The trailer type field from vehicle num: ' . $value . ' is required!',
                    'license' . $value . '.required' => 'The license field from vehicle num: ' . $value . ' is required!',
                    'locationStorage' . $value . '.required' => 'The location of storage field for vehicle num: ' . $value . ' is required!',
                    'deliveryDate' . $value . '.required' => 'The delivery date field for vehicle num: ' . $value . ' is required!',
                    'serviceMoment' . $value . '.required' => 'The set service moment field for vehicle num: ' . $value . ' is required!',
                    //'pickupDate' . $value . '.required' => 'The: ' . $value . ' is required!',
                );
            }


            $validator = Validator::make($fields, $rules, $messages);

            if ($validator->fails()) {
                return Response::json(array(
                            'success' => false,
                            'errors' => $validator->getMessageBag()->toArray()
                                ), 400);
            }
            // CHECK LICENSE
            $licenseCheck = Input::get('license');           
            foreach ($licenseCheck as $check) {               
                //$user = User::where('email', Input::get('email'))->first();
				$user = User::where('id', Input::get('id'))->first();                
                if (empty($user)) {                   
                    $vehicle = Vehicle::where('license', $check)->first();
                } else {
                    $vehicle = Vehicle::where('license', $check)->where('user_id', '!=', $user->id)->first();
                }
                if (!empty($vehicle)) {
                    return Response::json(array(
                                'success' => false,
                                'duplicate' => 'There is already license plate with these numbers: ' . $check
                                    ), 400);
                }
            }
            $trailer = array(
                'firstName' => ucfirst(Input::get('firstName')),
                'lastName' => ucfirst(Input::get('lastName')),
                'street' => Input::get('street'),
                'city' => Input::get('city'),
                'zip' => Input::get('zip'),
                'email' => Input::get('email'),
                'password' => 'secret',
                'password_confirmation' => 'secret',
                'username' => md5(time()) . mt_rand(5, 9999),
            );


            //$user_old = User::where('email', '=', Input::get('email'))->first();
			$user_old = User::where('id', '=', Input::get('id'))->first();
            if (isset($user_old)) {

                $user_old->firstName = ucfirst(Input::get('firstName'));
                $user_old->lastName = ucfirst(Input::get('lastName'));
                $user_old->zip = Input::get('zip');
                $user_old->city = ucfirst(Input::get('city'));
                $user_old->phone = Input::get('phone');
                $user_old->mobile = Input::get('mobile');
                $user_old->street = Input::get('street');
                $user_old->username = md5(time()) . mt_rand(5, 9999);
                $user_old->email = Input::get('email');
                $user_old->password = 'secret2';
                $user_old->password_confirmation = 'secret2';
                $user_old->confirmed = true;
                $user_old->save();
                $roles = array(2);
                $user_old->saveRoles($roles);
            } else {

                $user = User::create($trailer);
                $user->confirmed = true;
                $user->phone = Input::get('phone');
                $user->mobile = Input::get('mobile');
                $user->save();

                if ($user->save()) {
                    $roles = array(2);
                    $user->saveRoles($roles);
                } else {
                    return Redirect::to('admin/users/create')
                                    ->with('error', Lang::get('admin/users/messages.create.error'));
                }
            }

            if (isset($user_old)) {
                $res = array();
                $vehicles = Vehicle::where('user_id', '=', $user_old->id)->get();
                foreach ($vehicles as $key => $vehicle) {
                    $res[$key] = array('id' => $vehicle->id);
                    $vehicle->delete();
                }
            }

            foreach (Input::get('vehicleID') as $key => $value) {

                $vehicle = new Vehicle();
                $vehicle->trailerType = Input::get('trailerType')[$value];
                $vehicle->license = Input::get('license')[$value];
                $vehicle->locationStorage = Input::get('locationStorage')[$value];
                $data = Input::get('deliveryDate')[$value];
                $data = str_replace('/', '-', $data);
                $vehicle->deliveryDate = date("Y-m-d", strtotime($data));
                if (Input::get('serviceMoment')[$value]) {
                    $data2 = Input::get('serviceMoment')[$value];
                    $data2 = str_replace('/', '-', $data2);
                    $vehicle->serviceMoment = date("Y-m-d", strtotime($data2));
                }
                $data3 = Input::get('pickupDate')[$value];
                $data3 = str_replace('/', '-', $data3);
				/*
                if (strtotime($data3) < strtotime($data)) {
                    return Response::json(array(
                                'success' => false,
                                'errorsDate' => 'Pick-up date can NOT be less then delivery date!'
                                    ), 400);
                }

                if (isset($data2) && strtotime($data3) < strtotime($data2)) {
                    return Response::json(array(
                                'success' => false,
                                'errorsDate2' => 'The service moment can NOT be later then the pick-up date!'
                                    ), 400);
                }
				*/
                //$vehicle->pickupDate = date("Y-m-d", strtotime($data3));
                $vehicle->pickupDate = ($data3 ? date("Y-m-d", strtotime($data3)) : '');
				$vehicle->comments = Input::get('comments')[$value];
                $vehicle->park = Input::get('park')[$value];
                $vehicle->pick_up = Input::get('pick_up')[$value];



                if (!empty($user_old)) {
                    $vehicle->user_id = $user_old->id;
                } else {
                    $vehicle->user_id = $user->id;
                }


                $vehicle->save();

                if (isset($res) && count($res) > 0 && isset($res[$key])) {

                    $counts = Count::where('trailer_id', $res[$key]['id'])->get();

                    if (count($counts) > 0) {
                        foreach ($counts as $count) {
                            $count->trailer_id = $vehicle->id;
                            $count->save();
                        }
                    }
                }
                if ($vehicle->pick_up == 1) {
                    $counter = new Count();
                    $counter->trailer_id = $vehicle->id;
                    $counter->trailer_pick_up = 1;
                    $counter->save();
                }
            }

            return Response::json(array(
                        'success' => true
                            ), 200);
        }
    }

    public function autocomplete()
    {
        $term = Input::get('term');
        $results = array();
        $queries = DB::table('vehicles')
                        ->where('locationStorage', 'LIKE', '%' . $term . '%')
                        ->distinct()
                        ->groupBy('locationStorage')
                        ->take(5)->get();
        foreach ($queries as $query) {
            $results[] = [ 'id' => $query->id, 'value' => $query->locationStorage];
        }
        return Response::json($results);
    }

    public function destroy($id)
    {
        $counts = Count::where('trailer_id', $id)->get();
        foreach ($counts as $count) {
            $count->delete();
        }
        Vehicle::destroy($id);

        return Redirect::back()->with('success', Lang::get('admin/users/messages.delete.success2'));
        ;
    }

    /**
     * Display the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getShow($user)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($user)
    {
        if ($user->id) {
            $roles = $this->role->all();
            $permissions = $this->permission->all();
            $vehicles = Vehicle::where('user_id', '=', $user->id)->get();

            //$vehicles = $query->getItems(); 
//            echo '<pre>';
//            print_r($query);
//            exit();
            // Title
            $title = Lang::get('admin/users/title.user_update');
            // mode
            $mode = 'edit';

            return View::make('admin/users/create_edit', compact('user', 'roles', 'permissions', 'title', 'mode', 'vehicles'));
        } else {
            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.does_not_exist'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param User $user
     * @return Response
     */
    public function postEdit($user)
    {
        $oldUser = clone $user;
        $user->username = Input::get('firstName');
        $user->firstName = Input::get('firstName');
        $user->lastName = Input::get('lastName');
        $user->email = Input::get('email');
//        $user->confirmed = Input::get( 'confirm' );
        $user->phone = Input::get('phone');
        $user->mobile = Input::get('mobile');
        $user->street = Input::get('street');
        $user->city = Input::get('city');
        $user->zip = Input::get('zip');

        $password = 'secret';
        $passwordConfirmation = 'secret';

        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $user->password = $password;
                // The password confirmation will be removed from model
                // before saving. This field will be used in Ardent's
                // auto validation.
                $user->password_confirmation = $passwordConfirmation;
            } else {
                // Redirect to the new user page
                return Redirect::to('admin/users/' . $user->id . '/edit')->with('error', Lang::get('admin/users/messages.password_does_not_match'));
            }
        }

        if ($user->confirmed == null) {
            $user->confirmed = $oldUser->confirmed;
        }

        if ($user->save()) {
            // Save roles. Handles updating.
            $user->saveRoles(Input::get('roles'));
        } else {
            return Redirect::to('admin/users/' . $user->id . '/edit')
                            ->with('error', Lang::get('admin/users/messages.edit.error'));
        }

        // Get validation errors (see Ardent package)
        $error = $user->errors()->all();

        if (empty($error)) {
            // Redirect to the new user page
            return Redirect::to('admin/users/' . $user->id . '/edit')->with('success', Lang::get('admin/users/messages.edit.success'));
        } else {
            return Redirect::to('admin/users/' . $user->id . '/edit')->with('error', Lang::get('admin/users/messages.edit.error'));
        }
    }

    /**
     * Remove user page.
     *
     * @param $user
     * @return Response
     */
    public function getDelete($user)
    {
        // Title
        $title = Lang::get('admin/users/title.user_delete');

        // Show the page
        return View::make('admin/users/delete', compact('user', 'title'));
    }

    /**
     * Remove the specified user from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete($user)
    {

        $vehicles = Vehicle::where('user_id', $user->id)->get();
        foreach ($vehicles as $vehicle) {
            $counts = Count::where('trailer_id', $vehicle->id)->get();
            foreach ($counts as $count) {
                $count->delete();
            }
        }
        // Check if we are not trying to delete ourselves
        if ($user->id === Confide::user()->id) {
            // Redirect to the user management page
            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.delete.impossible'));
        }

        AssignedRoles::where('user_id', $user->id)->delete();

        $id = $user->id;
        $user->delete();



        // Was the comment post deleted?
        $user = User::find($id);
        if (empty($user)) {
            // TODO needs to delete all of that user's content
            return Redirect::to('admin/users')->with('success', Lang::get('admin/users/messages.delete.success'));
        } else {
            // There was a problem deleting the user
            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.delete.error'));
        }
    }

    /**
     * Show a list of all the users formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {


        if (Input::get('picked_up') == 'true') {
            $users = User::leftjoin('assigned_roles', 'assigned_roles.user_id', '=', 'users.id')
                    ->leftjoin('roles', 'roles.id', '=', 'assigned_roles.role_id')
                    ->leftjoin('vehicles', 'vehicles.user_id', '=', 'users.id')
                    ->select(array('users.id', 'users.username', 'users.firstName', 'users.lastName', 'vehicles.trailerType', 'vehicles.id as trailerId', 'vehicles.locationStorage', 'vehicles.pickupDate', 'vehicles.pick_up', 'users.confirmed', 'users.created_at'))
                    ->where('roles.name', 'trailer')
                    ->where('vehicles.pick_up', '1');
        } elseif (Input::get('picked_up') == 'false') {

            $users = User::leftjoin('assigned_roles', 'assigned_roles.user_id', '=', 'users.id')
                    ->leftjoin('roles', 'roles.id', '=', 'assigned_roles.role_id')
                    ->leftjoin('vehicles', 'vehicles.user_id', '=', 'users.id')
                    ->select(array('users.id', 'users.username', 'users.firstName', 'users.lastName', 'vehicles.trailerType', 'vehicles.id as trailerId', 'vehicles.locationStorage', 'vehicles.pickupDate', 'vehicles.pick_up', 'users.confirmed', 'users.created_at'))
                    ->where('roles.name', 'trailer')
                    ->where('vehicles.pick_up', '0');
        } else {
            $users = User::leftjoin('assigned_roles', 'assigned_roles.user_id', '=', 'users.id')
                    ->leftjoin('roles', 'roles.id', '=', 'assigned_roles.role_id')
                    ->leftjoin('vehicles', 'vehicles.user_id', '=', 'users.id')
                    ->select(array('users.id', 'users.username', 'users.firstName', 'users.lastName', 'vehicles.trailerType', 'vehicles.id as trailerId', 'vehicles.locationStorage', 'vehicles.pickupDate', 'vehicles.pick_up', 'users.confirmed', 'users.created_at'))
                    ->where('roles.name', 'trailer');
        }
		
		return Datatables::of($users)
                        // ->edit_column('created_at','{{{ Carbon::now()->diffForHumans(Carbon::createFromFormat(\'Y-m-d H\', $test)) }}}')
                        ->edit_column('confirmed', '@if($confirmed)
                            Yes
                        @else
                            No
                        @endif')
//                        ->edit_column('firstName', function($row){
//                             return $row->firstName .' '. $row->lastName;
//                        })
                        ->add_column('pick-up', '<?php $count = Count::where("trailer_id",  $trailerId)->where("created_at", ">", date("Y-m-d H:i:s",  strtotime("-1 year")))->get();?> {{count($count)}}')
						->add_column('actions', '
                                
                                    <a href="{{{ action(\'AdminUsersController@postDelete\', $id )}}}" class="delete-user btn btn-xs btn-danger action">{{{ Lang::get(\'button.delete\') }}}</a>
                                
            ')->edit_column('locationStorage', '@if($pick_up == 1)'
                                . '<span class="blue">Klant<span> '
                                . '@else'
                                . '{{$locationStorage}}'
                                . '@endif')
                        ->edit_column('pickupDate', '{{  (date_format(date_create($pickupDate), "Y") != "-0001" ? date_format(date_create($pickupDate), "d/m/Y") : null) }}')
                        ->edit_column('pick_up', '@if($pick_up)
                            <span style="color:#0270ca;" class="glyphicon glyphicon-ok">
                        @else
                           <span class="glyphicon glyphicon-minus">
                        @endif')
                        ->remove_column('trailerId', 'username', 'created_at', 'confirmed')
                        ->make();
    }

    public function getServiceData()
    {
        $users = User::leftjoin('assigned_roles', 'assigned_roles.user_id', '=', 'users.id')
                ->leftjoin('roles', 'roles.id', '=', 'assigned_roles.role_id')
                ->leftjoin('vehicles', 'vehicles.user_id', '=', 'users.id')
                ->select(array('users.id', 'users.username', 'users.firstName', 'users.lastName', 'vehicles.trailerType', 'vehicles.locationStorage', 'vehicles.pickupDate', 'vehicles.pick_up', 'vehicles.serviceMoment', 'users.created_at'))
                ->where('roles.name', 'trailer')
                ->whereNotNull('serviceMoment');


        return Datatables::of($users)
                        ->edit_column('locationStorage', '@if($pick_up == 1)'
                                . '<span class="blue">Klant<span> '
                                . '@else'
                                . '{{$locationStorage}}'
                                . '@endif')
                        ->edit_column('pickupDate', '{{  (date_format(date_create($pickupDate), "Y") != "-0001" ? date_format(date_create($pickupDate), "d/m/Y") : null }}')
                        ->edit_column('serviceMoment', '{{  date_format(date_create($serviceMoment), "d/m/Y") }}')
                        ->edit_column('pick_up', '@if($pick_up)
                            <span style="color:#0270ca;" class="glyphicon glyphicon-ok">
                        @else
                           <span class="glyphicon glyphicon-minus">
                        @endif')
                        ->remove_column('id', 'username', 'created_at')
                        ->make();
    }

}
