$(".nav-tabs").on("click", "a", function (e) {
        e.preventDefault();
        if (!$(this).hasClass('add-contact')) {
            $(this).tab('show');
           
        }
    })
    .on("click", "span", function () {
        var anchor = $(this).siblings('a');
        $(anchor.attr('href')).remove();
        $(this).parent().remove();
        $(".nav-tabs li").children('a').first().click();
    });

$('.add-contact').click(function (e) {
    e.preventDefault();
    
    var id = $(".nav-tabs").children().length - 1; //think about it ;)
    var plus = id + 1;
    var tabId = 'contact_' + id;
    $(this).closest('li').before('<li><a href="#contact_' + id + '">Vehicle ' + id +'</a> <span> x </span></li>');
    
    $('.tab-content').append('<div class="tab-pane" id="' + tabId + '"><div class="form-group">\n\
<label class="col-md-2 control-label" for="trailerType">Type of trailer</label>\n\
<div class="col-md-10"><input class="form-control" type="text" name="trailerType[' + id + ']" id="trailerType' + id + '" value=""  />\n\
<input type="hidden" name="vehicleID[]" value="' + id + '"/>\n\
</div>\n\
</div>\n\
\n\
<div class="form-group" >\n\
<label class="col-md-2 control-label" for="deliveryDate">Delivery date</label>\n\
<div  class="col-md-3 input-group date" id="deliveryDate' + plus + '">\n\
<input  type="text" id="deliveryDate" name="deliveryDate[' + id + ']" class="form-control" />\n\
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div>\n\
</div>\n\
\n\
<div class="form-group" >\n\
<label class="col-md-2 control-label" for="locationStorage">Location of storage</label>\n\
<div class="col-md-10"><input class="form-control search" type="text" name="locationStorage[' + id + ']" id="locationStorage" value="" /></div>\n\
</div>\n\
\n\
<div class="form-group" >\n\
<label class="col-md-2 control-label" for="serviceMoment">Set Service moment</label>\n\
<div  class="col-md-3 input-group date" id="serviceMoment' + plus + '">\n\
<input  type="text" id="serviceMoment" name="serviceMoment[' + id + ']" class="form-control" />\n\
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div>\n\
</div>\n\
\n\
<div class="form-group" >\n\
<label class="col-md-2 control-label" for="license">License</label>\n\
<div class="col-md-10"><input class="form-control" type="text" name="license[' + id + ']" id="license" value="" /></div>\n\
</div>\n\
<div class="form-group" >\n\
<label class="col-md-2 control-label" for="park">Park for year</label>\n\
<div class="btn-group date" data-toggle="buttons">\n\
<select class="form-control" name="park[' + id + ']">\n\
<option value="1" >Yes</option>\n\
<option value="0" selected="selected">No</option>\n\
</select>\n\
</div>\n\
</div>\n\
<div class="form-group" >\n\
<label class="col-md-2 control-label" for="pickupDate">Pickup date</label>\n\
<div  class="col-md-3 input-group date" id="pickupDate' + plus + '">\n\
<input  type="text" id="pickupDate" name="pickupDate[' + id + ']" class="form-control" />\n\
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div>\n\
</div>\n\
\n\
<div class="form-group" >\n\
<label class="col-md-2 control-label" for="serviceMoment">Comment</label>\n\
<div class="col-md-10"><textarea name="comments[' + id + ']" class="form-control" rows="3"></textarea></div>\n\
</div>\n\
<div class="form-group">\n\
<label class="col-md-2 control-label" for="comments">Confirm Pick up</label>\n\
<div class="checkbox col-md-10 date">\n\
<input type="hidden" name="pick_up[' + id + ']" value="0">\n\
<input type="checkbox" name="pick_up[' + id + ']" value="1">\n\
</div>\n\
</div>\n\
</div>');
    
   $('.nav-tabs li:nth-child(' + id + ') a').click();
   $('#deliveryDate' + plus).datetimepicker({minDate: moment(), format: 'DD/MM/YYYY'});
   $('#serviceMoment' + plus).datetimepicker({minDate: moment(), format: 'DD/MM/YYYY'});
   $('#pickupDate' + plus).datetimepicker({minDate: moment().add(1, 'd').toDate(), format: 'DD/MM/YYYY'});
   


   
});

                     