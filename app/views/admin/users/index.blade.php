@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ $title }}} :: @parent
@stop

{{-- Content --}}
@section('content')

<div class="page-header">
    <h2>
        {{{ $title }}}
        <a href="{{{ URL::to('admin/users') }}}" class="btn btn-small  "><span class="glyphicon glyphicon-eye-open"></span> All Vehicles</a>
        <a href="{{{ URL::to('admin/users?picked_up=true') }}}" class="btn btn-small "><span class="glyphicon glyphicon glyphicon-ok"></span> Picked Up Vehicles</a>
        <a href="{{{ URL::to('admin/users?picked_up=false') }}}" class="btn btn-small  "><span class="glyphicon glyphicon-minus"></span> Not Picked Up Vehicles</a>
        <div class="pull-right">
            <a href="{{{ URL::to('admin/users/service-moment') }}}" class="btn btn-small btn-default "><span class="glyphicon glyphicon-cog"></span> Service Moment</a>
            <a href="{{{ URL::to('admin/users/create') }}}" class="btn btn-small btn-info "><span class="glyphicon glyphicon-plus-sign"></span> Register Trailer</a>


        </div>
    </h2>
</div>

<table id="users" class="table table-striped table-hover">
    <thead>
        <tr>
            <th class="col-md-1">#</th>
            <th class="col-md-2">{{{ Lang::get('admin/users/table.first_name') }}}</th>
            <th class="col-md-2">{{{ Lang::get('admin/users/table.last_name') }}}</th>
            <th class="col-md-2">{{{ Lang::get('admin/users/table.trailerType') }}}</th>
            <th class="col-md-2">{{{ Lang::get('admin/users/table.location') }}}</th>
            <th class="col-md-1">{{{ Lang::get('admin/users/table.pick_up_date') }}}</th>
            <th class="col-md-1">{{{ Lang::get('admin/users/table.confirm') }}}</th>
            <th class="col-md-1">Pick-up <br/> counter</th>
            <th class="col-md-1">{{{ Lang::get('table.actions') }}}</th>

        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@stop

{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
    var oTable;
    $(document).ready(function () {
        oTable = $('#users').dataTable({
            "responsive":true,
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ records per page"
            },
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "/admin/users/data?picked_up=<?php echo $pick_up; ?>",
            "aoColumnDefs": [
                {"bSearchable": false, "bVisible": false, "aTargets": [0]}
            ],
            "fnDrawCallback": function (oSettings) {
                $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
                $('#users tbody tr').click(function (evt) {
                    // get position of the selected row
                    var position = oTable.fnGetPosition(this);
                    // value of the first column (can be hidden)
                    var id = oTable.fnGetData(position)[0];
                    // redirect
                       var $cell=$(evt.target).closest('td'), msg;
                      $cell.index()>6  ? '' : document.location.href = "/admin/users/" + id + "/edit";
                      
                });

            }


        });

        

    });




</script>
@stop