<?php

class UserController extends BaseController
{

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * @var Vehicle
     */
    protected $vehicle;

    /**
     * Inject the models.
     * @param User $user
     * @param UserRepository $userRepo
     */
    public function __construct(User $user, UserRepository $userRepo, Vehicle $vehicle)
    {
        parent::__construct();
        $this->user = $user;
        $this->userRepo = $userRepo;
        $this->vehicle = $vehicle;
    }

    /**
     * Users settings page
     *
     * @return View
     */
    public function getIndex()
    {
        list($user, $redirect) = $this->user->checkAuthAndRedirect('user');
        if ($redirect) {
            return $redirect;
        }

        $vehicles = Vehicle::where('user_id', '=', $user->id)->get();

        // Show the page
        return View::make('site/user/index', compact('user', 'vehicles'));
    }

    /**
     * Stores new user
     *
     */
    public function postIndex()
    {
        $user = $this->userRepo->signup(Input::all());

        if ($user->id) {
            if (Config::get('confide::signup_email')) {
                Mail::queueOn(
                        Config::get('confide::email_queue'), Config::get('confide::email_account_confirmation'), compact('user'), function ($message) use ($user) {
                    $message
                            ->to($user->email, $user->username)
                            ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
                }
                );
            }

            return Redirect::to('user/login')
                            ->with('success', Lang::get('user/user.user_account_created'));
        } else {
            $error = $user->errors()->all(':message');

            return Redirect::to('user/create')
                            ->withInput(Input::except('password'))
                            ->with('error', $error);
        }
    }

    public function postDate()
    {
        $user = Auth::user();
        $vehicle = Vehicle::where('user_id', $user->id)->where('id', Input::get('id'))->first();


        if (Input::get('serviceMomentUser')) {
            $serviceMoment = Input::get('serviceMomentUser');
            $serviceMoment = str_replace('/', '-', $serviceMoment);
            $vehicle->serviceMoment = date("Y-m-d", strtotime($serviceMoment));
//            $serviceMoment = Input::get('serviceMomentUser');
            $pickupDate = $vehicle->pickupDate;
            $pickupDate = date_create($pickupDate);
            $pickupDate = date_format($pickupDate, 'd/m/Y');
            $pickupDate = str_replace('/', '-', $pickupDate);

            if (strtotime($pickupDate) < strtotime($serviceMoment)) {
                return Redirect::back()->with('error', 'De veiligheidscontrole / onderhoudsbeurt kan niet later zijn dan het ophaalmoment!');
            }
        } else if (Input::get('pickupDateUser')) {
            $pickupDate = Input::get('pickupDateUser');
            $pickupDate = str_replace('/', '-', $pickupDate);
            $vehicle->pickupDate = date("Y-m-d", strtotime($pickupDate));
            $deliveryDate = $vehicle->deliveryDate;
            $deliveryDate = date_create($deliveryDate);
            $deliveryDate = date_format($deliveryDate, 'd/m/Y');
            $deliveryDate = str_replace('/', '-', $deliveryDate);
            // date check
            if (strtotime($pickupDate) < strtotime($deliveryDate)) {
                return Redirect::back()->with('error', 'Het ophaalmoment kan niet eerder zijn dan de bezorgdatum!');
            }
        }
        $vehicle->save();
        if ($vehicle->save()) {
            if (isset($serviceMoment)) {
                $date = $serviceMoment;
                $type = 'Service Moment';
            } else {
                $date = $pickupDate;
                $type = 'Pick-up Date';
            }
            Mail::send('emails.reschedulingEmail', array('firstname' => $user->firstName, 'lastname' => $user->lastName,
                'phone' => $user->phone, 'street' => $user->street, 'email' => $user->email, 'city' => $user->city, 'zip' => $user->zip,
                'trailerType' => $vehicle->trailerType, 'license' => $vehicle->license, 'locationStorage' => $vehicle->locationStorage,
                'type' => $type, 'date' => $date), function($message) use ($user) {
                $message->to('info@deklerkcaravans.nl', 'DeKlerkCaravans')->subject('Email from DeKlerkCaravans');
            });


            return Redirect::back()->with('success', Lang::get('user/user.user_account_updated'));
        } else {
            return Redirect::back()->with('error', 'There was an issue deleting the user. Please try again.');
        }
    }

    /**
     * Edits a user
     * @var User
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(User $user)
    {
        $oldUser = clone $user;

        $user->username = Input::get('username');
        $user->email = Input::get('email');
        $user->firstName = Input::get('firstName');
        $user->lastName = Input::get('lastName');

        $password = Input::get('password');
        $passwordConfirmation = Input::get('password_confirmation');

        if (!empty($password)) {
            if ($password != $passwordConfirmation) {
                // Redirect to the new user page
                $error = Lang::get('admin/users/messages.password_does_not_match');
                return Redirect::to('user')
                                ->with('error', $error);
            } else {
                $user->password = $password;
                $user->password_confirmation = $passwordConfirmation;
            }
        }

        if ($this->userRepo->save($user)) {
            return Redirect::to('user')
                            ->with('success', Lang::get('user/user.user_account_updated'));
        } else {
            $error = $user->errors()->all(':message');
            return Redirect::to('user')
                            ->withInput(Input::except('password', 'password_confirmation'))
                            ->with('error', $error);
        }
    }

    /**
     * Displays the form for user creation
     *
     */
    public function getCreate()
    {
        return View::make('site/user/create');
    }

    /**
     * Displays the login form
     *
     */
    public function getLogin()
    {
        $user = Auth::user();
        if (!empty($user->id)) {
            return Redirect::to('/');
        }

        return View::make('site/user/login');
    }

    /**
     * Attempt to do login
     *
     */
    public function postLogin()
    {
        $repo = App::make('UserRepository');
        $input = Input::all();

        if ($this->userRepo->login($input)) {
            if (Auth::user()->hasRole('admin')) {
                return Redirect::intended('/admin/users');
            }
            return Redirect::action('UserController@getLogout');
        } else {
            if ($this->userRepo->isThrottled($input)) {
                $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
            } elseif ($this->userRepo->existsButNotConfirmed($input)) {
                $err_msg = Lang::get('confide::confide.alerts.not_confirmed');
            } else {
                $err_msg = Lang::get('confide::confide.alerts.wrong_credentials');
            }
            $sas = Input::get('sas');
            if (isset($sas)) {
                return Redirect::to('user/login/admin')
                                ->withInput(Input::except('password'))
                                ->with('error', $err_msg);
            }
            return Redirect::to('user/login')
                            ->withInput(Input::except('password'))
                            ->with('error', $err_msg);
        }
    }

    public function postLoginEmail()
    {
        $data = Input::get('email');
        $user = User::where('email', '=', $data)->first();
        $err_msg2 = Lang::get('confide::confide.alerts.wrong_credentials');

        if (empty($user)) {
            $user = Vehicle::where('license', '=', $data)->first();
            if ($user) {

                Auth::loginUsingId($user->user_id);
                if (Auth::user()->hasRole('admin')) {
                    return Redirect::action('UserController@getLogout')->with('error', $err_msg2);
                }
                // if user has role admin redirect to admin dashboard
//                if (Auth::user()->hasRole('admin')) {
//                    return Redirect::intended('/admin');
//                }
                return Redirect::intended('/user');
            }
        } else {

            if ($user) {
                Auth::loginUsingId($user->id);
                if (Auth::user()->hasRole('admin')) {
                    return Redirect::action('UserController@getLogout')->with('error', $err_msg2);
                }
                // if user has role admin redirect to admin dashboard
//                if (Auth::user()->hasRole('admin')) {
//                    return Redirect::intended('/admin');
//                }
                return Redirect::intended('/user');
            }
        }
        return Redirect::back()->with('error', $err_msg2);
    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getConfirm($code)
    {
        if (Confide::confirm($code)) {
            return Redirect::to('user/login')
                            ->with('notice', Lang::get('confide::confide.alerts.confirmation'));
        } else {
            return Redirect::to('user/login')
                            ->with('error', Lang::get('confide::confide.alerts.wrong_confirmation'));
        }
    }

    /**
     * Displays the forgot password form
     *
     */
    public function getForgot()
    {
        return View::make('site/user/forgot');
    }

    /**
     * Attempt to reset password with given email
     *
     */
    public function postForgotPassword()
    {
        if (Confide::forgotPassword(Input::get('email'))) {
            $notice_msg = Lang::get('confide::confide.alerts.password_forgot');
            return Redirect::to('user/forgot')
                            ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');
            return Redirect::to('user/login')
                            ->withInput()
                            ->with('error', $error_msg);
        }
    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function getReset($token)
    {

        return View::make('site/user/reset')
                        ->with('token', $token);
    }

    /**
     * Attempt change password of the user
     *
     */
    public function postReset()
    {

        $input = array(
            'token' => Input::get('token'),
            'password' => Input::get('password'),
            'password_confirmation' => Input::get('password_confirmation'),
        );

        // By passing an array with the token, password and confirmation
        if ($this->userRepo->resetPassword($input)) {
            $notice_msg = Lang::get('confide::confide.alerts.password_reset');
            return Redirect::to('user/login')
                            ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
            return Redirect::to('user/reset', array('token' => $input['token']))
                            ->withInput()
                            ->with('error', $error_msg);
        }
    }

    /**
     * Log the user out of the application.
     *
     */
    public function getLogout()
    {
        Confide::logout();

        return Redirect::to('/');
    }

    /**
     * Get user's profile
     * @param $username
     * @return mixed
     */
    public function getProfile($username)
    {
        $userModel = new User;
        $user = $userModel->getUserByUsername($username);

        // Check if the user exists
        if (is_null($user)) {
            return App::abort(404);
        }

        return View::make('site/user/profile', compact('user'));
    }

    public function getSettings()
    {
        list($user, $redirect) = User::checkAuthAndRedirect('user/settings');
        if ($redirect) {
            return $redirect;
        }

        return View::make('site/user/profile', compact('user'));
    }

    /**
     * Process a dumb redirect.
     * @param $url1
     * @param $url2
     * @param $url3
     * @return string
     */
    public function processRedirect($url1, $url2, $url3)
    {
        $redirect = '';
        if (!empty($url1)) {
            $redirect = $url1;
            $redirect .= (empty($url2) ? '' : '/' . $url2);
            $redirect .= (empty($url3) ? '' : '/' . $url3);
        }
        return $redirect;
    }

}
